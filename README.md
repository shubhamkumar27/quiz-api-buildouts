# Quiz API - Buildouts

A simple REST API application, build on Java - Spring Boot.

This Spring boot application acts as a Quiz API, where it provides Questions and their options according to the moduleId given in API. It also accepts answers from the user through a POST request and then returns back all Questions with their options, correct answers and the score of the user.

### NOTE
This project was built during Crio Launch Plus program while Learning about Java and Spring. So it also consits of several files which were given by Crio, such as External build.gradle, prepare-server.sh, JSON resources and other assessment related files.

**builout** folder contains all the work i have done in this project.

### Motive of this project
1) Learning about Spring Boot and Java
1) Following MVC Pattern
2) Writing Unit Tests
3) Writing Modular Code

### Local Setup
1) Clone the Repository on your machine.
2) Make sure you have Java and MongoDB installed on your system.
3) Open up terminal inside project directory.
4) Write command `./prepare-server.sh`. If it's not executable, then run `chmod +x prepare-server.sh` before given command.
5) Your Spring server will get started on localhost:8081.

### Workflow
1) We can get all the Questions of a module by making a GET request on the given API :

``` http://localhost:8081/quiz/{moduleId}/ ```
(Currently, it only works for moduleId : 1)

When you hit this API, you will get the JSON file consisting of all Questions and their options.

2) If the user wants to submit the answers of the Questions, he can make a POST request on the same url with a JSON data of marked answers. Example of the JSON data to be provided in POST request.
```
{
  "responses": [
    {
      "questionId": "001",
      "userResponse": ["1"]
    },
    {
      "questionId": "002",
      "userResponse": ["1", "3", "4"]
    },
    {
      "questionId": "003",
      "userResponse": ["throwable"]
    }
  ]
}
``` 

3) When the user submits his answers, the API returns back a JSON file again which consists of all the Questions, their options, correct answers and the score of the user.

