package com.crio.buildouts.controller;

import com.crio.buildouts.dto.QuestionDetails;
import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.crio.buildouts.exchanges.SubmitQuestionRequest;
import com.crio.buildouts.exchanges.SubmitQuestionResponse;
import com.crio.buildouts.services.QuizService;

import java.util.List;
import javax.validation.Valid;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(QuizController.QUIZ_API_ENDPOINT)
public class QuizController {

  public static final String QUIZ_API_ENDPOINT = "/quiz/{moduleId}";

  @Autowired
  private QuizService quizservice;

  @PutMapping
  public ResponseEntity putQuestions(@PathVariable String moduleId,
      @RequestBody List<QuestionDetails> questions) {
    quizservice.putQuestions(moduleId, questions);
    return ResponseEntity.ok().build();
  }

  @GetMapping
  public ResponseEntity<GetQuestionResponse> getQuestions(@PathVariable String moduleId) {
    GetQuestionResponse response = quizservice.getQuestions(moduleId);
    return ResponseEntity.ok().body(response);
  }

  @PostMapping
  public ResponseEntity<SubmitQuestionResponse> submitQuestions(@PathVariable String moduleId,
      @Valid @RequestBody SubmitQuestionRequest request) {
    SubmitQuestionResponse response = quizservice.getSubmitResponse(moduleId, request);
    return ResponseEntity.ok().body(response);
  }

}