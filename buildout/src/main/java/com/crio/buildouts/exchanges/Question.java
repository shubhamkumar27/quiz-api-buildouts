package com.crio.buildouts.exchanges;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class Question {

  /**
   * { "questionId": "001", "title": "What is the default IP address of
   * localhost?", "description": "General question", "type": "objective-single",
   * "options": { "1": "0.0.0.0", "2": "192.168.0.12", "3": "127.0.0.1", "4":
   * "255.255.255.255" }, "correctAnswer": [ "4" ] }
   */
  
  private String questionId;
  private String title;
  private String type;
  private Object options;
}