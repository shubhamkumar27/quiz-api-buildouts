package com.crio.buildouts.exchanges;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@Builder
public class QuestionResponse {

  /**
   * { "questionId": "001", "title": "What is the default IP address of
   * localhost?", "description": "General question", "type": "objective-single",
   * "options": { "1": "0.0.0.0", "2": "192.168.0.12", "3": "127.0.0.1", "4":
   * "255.255.255.255" }, "userAnswer": [ "1" ], "correct": [ "4" ],
   * "explanation": null, "answerCorrect": false }
   */

  private String questionId;
  private String title;
  private String description;
  private String type;
  private Object options;
  private List<String> userAnswer;
  private List<String> correct;
  private String explanation;
  private boolean answerCorrect;

}