package com.crio.buildouts.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "questions")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class QuestionEntity {

  @Id
  private String questionId;

  @NotNull
  private String title;

  @NotNull
  private String description;

  @NotNull
  private String moduleId;

  @NotNull
  private String type;

  @NotNull
  private Object options;

  @NotNull
  private List<String> correctAnswer;

}