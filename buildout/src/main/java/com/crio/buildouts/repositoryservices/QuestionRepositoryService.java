package com.crio.buildouts.repositoryservices;

import com.crio.buildouts.models.QuestionEntity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionRepositoryService {

  @Autowired
  private QuestionRepository questionRepository;

  public void saveQuestions(List<QuestionEntity> entities) {
    questionRepository.saveAll(entities);
  }

  public List<QuestionEntity> findByModuleId(String moduleId) {
    return questionRepository.findByModuleId(moduleId);
  }

}