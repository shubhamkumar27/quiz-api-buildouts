package com.crio.buildouts.services;

import com.crio.buildouts.dto.QuestionDetails;
import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.crio.buildouts.exchanges.SubmitQuestionRequest;
import com.crio.buildouts.exchanges.SubmitQuestionResponse;

import java.util.List;

public interface QuizService {

  void putQuestions(String moduleId, List<QuestionDetails> questions);

  GetQuestionResponse getQuestions(String moduleId);
  
  SubmitQuestionResponse getSubmitResponse(String moduleId, SubmitQuestionRequest request);

}