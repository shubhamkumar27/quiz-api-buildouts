package com.crio.buildouts.services;

import com.crio.buildouts.dto.QuestionDetails;
import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.crio.buildouts.exchanges.Question;
import com.crio.buildouts.exchanges.QuestionResponse;
import com.crio.buildouts.exchanges.SubmitQuestionRequest;
import com.crio.buildouts.exchanges.SubmitQuestionResponse;
import com.crio.buildouts.exchanges.Summary;
import com.crio.buildouts.exchanges.UserResponse;
import com.crio.buildouts.models.QuestionEntity;
import com.crio.buildouts.repositoryservices.QuestionRepositoryService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuizServiceImpl implements QuizService {

  @Autowired
  private QuestionRepositoryService questionRepositoryService;

  @Override
  public void putQuestions(String moduleId, List<QuestionDetails> questions) {
    List<QuestionEntity> entities = new ArrayList<>();
    for (QuestionDetails question : questions) {
      QuestionEntity entity = QuestionEntity.builder()
          .moduleId(moduleId)
          .correctAnswer(question.getCorrectAnswer())
          .description(question.getDescription())
          .options(question.getOptions())
          .questionId(question.getQuestionId())
          .title(question.getTitle())
          .type(question.getType())
          .build();
      entities.add(entity);
    }
    questionRepositoryService.saveQuestions(entities);
  }

  @Override
  public GetQuestionResponse getQuestions(String moduleId) {
    ModelMapper modelMapper = new ModelMapper();
    List<QuestionEntity> questionEntities = questionRepositoryService.findByModuleId(moduleId);
    List<Question> questions = new ArrayList<>();
    for (QuestionEntity entity : questionEntities) {
      Question question = modelMapper.map(entity, Question.class);
      questions.add(question);
    }
    GetQuestionResponse response = new GetQuestionResponse(questions);
    return response;
  }

  @Override
  public SubmitQuestionResponse getSubmitResponse(String moduleId, SubmitQuestionRequest request) {
    List<QuestionEntity> questionEntities = questionRepositoryService.findByModuleId(moduleId);
    List<QuestionResponse> questionResponses = new ArrayList<>();
    HashMap<String, QuestionEntity> questions = new HashMap<>();
    int score = 0;
    for (QuestionEntity question : questionEntities) {
      questions.put(question.getQuestionId(), question);
    }
    for (UserResponse response : request.getResponses()) {
      QuestionEntity entity = questions.get(response.getQuestionId());
      QuestionResponse questionResponse = QuestionResponse.builder()
          .questionId(entity.getQuestionId())
          .title(entity.getTitle())
          .description(entity.getDescription())
          .type(entity.getType())
          .options(entity.getOptions())
          .correct(entity.getCorrectAnswer())
          .explanation(null)
          .userAnswer(response.getUserResponse())
          .answerCorrect(false).build();
      if (checkAnswer(response.getUserResponse(), entity.getCorrectAnswer())) {
        score += 1;
        questionResponse.setAnswerCorrect(true);
      }
      questionResponses.add(questionResponse);
    }
    Summary summary = new Summary(score, request.getResponses().size());
    SubmitQuestionResponse response = new SubmitQuestionResponse(questionResponses, summary);
    return response;
  }

  private boolean checkAnswer(List<String> userResponse, List<String> correctAnswers) {
    if (userResponse.size() != correctAnswers.size()) {
      return false;
    }
    Collections.sort(userResponse);
    Collections.sort(correctAnswers);
    for (int i = 0; i < userResponse.size(); i++) {
      if (!userResponse.get(i).equals(correctAnswers.get(i))) {
        return false;
      }
    }
    return true;
  }

}