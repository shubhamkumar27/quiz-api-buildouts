package com.crio.buildouts.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.crio.buildouts.exchanges.SubmitQuestionRequest;
import com.crio.buildouts.exchanges.SubmitQuestionResponse;
import com.crio.buildouts.services.QuizService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
public class QuizControllerTest {

  protected static final String INITIALJSON = "fixtures/initial_data_load.json";
  protected static final String SAMPLEQUESTIONS = "fixtures/sample_get_questions_response.json";
  protected static final String SUBMITREQUEST = "fixtures/sample_submit_question_request.json";
  protected static final String SUBMITRESPONSE = "fixtures/sample_submit_question_response.json";
  
  private static final String QUIZ_API_ENDPOINT = "/quiz/";
  private static final String MODULE_ID = "1";

  private MockMvc mvc;
  private ObjectMapper objectMapper;

  @Mock
  private QuizService quizService;

  @InjectMocks
  private QuizController quizController;

  @BeforeEach
  public void setup() {
    objectMapper = new ObjectMapper();
    MockitoAnnotations.initMocks(this);
    mvc = MockMvcBuilders.standaloneSetup(quizController).build();
  }

  @Test
  public void putDataInDatabaseTest() throws Exception {
    String jsonValue = resolveFileAsString(INITIALJSON);

    URI uri = UriComponentsBuilder
        .fromPath(QUIZ_API_ENDPOINT + MODULE_ID)
        .build().toUri();

    assertEquals(QUIZ_API_ENDPOINT + MODULE_ID, uri.toString());

    MockHttpServletResponse response = mvc.perform(
        put(uri.toString())
        .contentType(APPLICATION_JSON_VALUE)
        .content(jsonValue)
    ).andReturn().getResponse();

    assertEquals(HttpStatus.OK.value(), response.getStatus());

  }

  @Test
  public void getDataFromDatabaseTest() throws Exception {
    String jsonValue = resolveFileAsString(SAMPLEQUESTIONS);

    GetQuestionResponse questionResponse = objectMapper.readValue(
        jsonValue, GetQuestionResponse.class);

    when(quizService.getQuestions(anyString()))
        .thenReturn(questionResponse);

    URI uri = UriComponentsBuilder
        .fromPath(QUIZ_API_ENDPOINT + MODULE_ID)
        .build().toUri();

    assertEquals(QUIZ_API_ENDPOINT + MODULE_ID, uri.toString());

    MockHttpServletResponse response = mvc.perform(
        get(uri.toString())
        .accept(APPLICATION_JSON)
    ).andReturn().getResponse();

    assertEquals(HttpStatus.OK.value(), response.getStatus());
    JSONAssert.assertEquals(jsonValue, response.getContentAsString(), true);
  }

  @Test
  public void postAnswerRequestTest() throws Exception {
    String request = resolveFileAsString(SUBMITREQUEST);
    String jsonResponse = resolveFileAsString(SUBMITRESPONSE);

    SubmitQuestionResponse questionResponse = objectMapper.readValue(
        jsonResponse, SubmitQuestionResponse.class);

    when(quizService.getSubmitResponse(anyString(), any(SubmitQuestionRequest.class)))
        .thenReturn(questionResponse);

    URI uri = UriComponentsBuilder
        .fromPath(QUIZ_API_ENDPOINT + MODULE_ID)
        .build().toUri();

    assertEquals(QUIZ_API_ENDPOINT + MODULE_ID, uri.toString());

    MockHttpServletResponse response = mvc.perform(
        post(uri.toString())
        .accept(APPLICATION_JSON)
        .contentType(APPLICATION_JSON_VALUE)
        .content(request)
    ).andReturn().getResponse();

    assertEquals(HttpStatus.OK.value(), response.getStatus());
    JSONAssert.assertEquals(jsonResponse, response.getContentAsString(), true);
  }

  private String resolveFileAsString(String input) {
    final URL resource = Resources.getResource(input);
    try {
      return Resources.toString(resource, StandardCharsets.UTF_8).trim();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}