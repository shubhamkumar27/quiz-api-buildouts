package com.crio.buildouts.exchanges;

import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

// TODO: CRIO_TASK_MODULE_SERIALIZATION - Pass tests in RestaurantTest.
class GetQuestionResponseTest {

  @Test
  public void serializeAndDeserializeJson() throws IOException, JSONException {
    final String jsonString = resolveFileAsString("fixtures/sample_get_questions_response.json");

    GetQuestionResponse response = new GetQuestionResponse();
    response = new ObjectMapper().readValue(jsonString, GetQuestionResponse.class);

    String actualJsonString = "";
    actualJsonString = new ObjectMapper().writeValueAsString(response);
    JSONAssert.assertEquals(jsonString, actualJsonString, true);
  }

  private String resolveFileAsString(String file) {
    final URL resource = Resources.getResource(file);
    try {
      return Resources.toString(resource, StandardCharsets.UTF_8).trim();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }

}