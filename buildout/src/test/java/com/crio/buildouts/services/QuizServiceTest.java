package com.crio.buildouts.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.crio.buildouts.dto.QuestionDetails;
import com.crio.buildouts.exchanges.GetQuestionResponse;
import com.crio.buildouts.exchanges.SubmitQuestionRequest;
import com.crio.buildouts.exchanges.SubmitQuestionResponse;
import com.crio.buildouts.models.QuestionEntity;
import com.crio.buildouts.repositoryservices.QuestionRepositoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@ExtendWith(MockitoExtension.class)
class QuizServiceTest {

  protected static final String INITIALJSON = "fixtures/initial_data_load.json";
  protected static final String SAMPLEQUESTIONS = "fixtures/sample_get_questions_response.json";
  protected static final String SUBMITREQUEST = "fixtures/sample_submit_question_request.json";
  protected static final String SUBMITRESPONSE = "fixtures/sample_submit_question_response.json";
  
  protected List<QuestionDetails> questions;
  protected List<QuestionEntity> entities = new ArrayList<>();

  @Mock
  protected QuestionRepositoryService questionRepositoryServiceMock;
  protected ObjectMapper objectMapper;

  @InjectMocks
  protected QuizServiceImpl quizService;

  @BeforeEach
  void setup() {
    MockitoAnnotations.initMocks(this);  
    objectMapper = new ObjectMapper();
    // initialiseQuestionsList();
  }

  public void initialiseQuestionsList() throws JsonMappingException, JsonProcessingException {
    String jsonString = resolveFileAsString(INITIALJSON);
    questions = objectMapper.readValue(jsonString, 
        new TypeReference<List<QuestionDetails>>() {});
  }

  public void initialiseEntities(String moduleId) {
    for (QuestionDetails question : questions) {
      QuestionEntity entity =  QuestionEntity.builder()
          .moduleId(moduleId)
          .correctAnswer(question.getCorrectAnswer())
          .description(question.getDescription())
          .options(question.getOptions())
          .questionId(question.getQuestionId())
          .title(question.getTitle())
          .type(question.getType())
          .build();
      entities.add(entity);
    }
  }

  @Test
  public void testPutQuestionService() throws JsonMappingException, JsonProcessingException {
    initialiseQuestionsList();
    quizService.putQuestions("1", questions);
    verify(questionRepositoryServiceMock, times(1)).saveQuestions(anyList());
  }

  @Test
  public void testGetQuestionService() throws JsonMappingException, JsonProcessingException {
    initialiseQuestionsList();
    initialiseEntities("1");
    when(questionRepositoryServiceMock
        .findByModuleId(anyString())).thenReturn(entities);
    GetQuestionResponse actualResponse = quizService.getQuestions("1");
    assertEquals(3, actualResponse.getQuestions().size());
    assertEquals("001", actualResponse.getQuestions().get(0).getQuestionId());
    assertEquals("002", actualResponse.getQuestions().get(1).getQuestionId());
    assertEquals("003", actualResponse.getQuestions().get(2).getQuestionId());
  }

  @Test
  public void testGetSubmitResponse() throws JsonMappingException, JsonProcessingException {
    initialiseQuestionsList();
    initialiseEntities("1");
    when(questionRepositoryServiceMock
        .findByModuleId(anyString())).thenReturn(entities);
    SubmitQuestionRequest submitRequest = objectMapper.readValue(
        resolveFileAsString(SUBMITREQUEST), SubmitQuestionRequest.class);
    SubmitQuestionResponse actualResponse = quizService.getSubmitResponse("1", submitRequest);
    assertEquals(Integer.valueOf(2), actualResponse.getSummary().getScore());
    assertEquals(Integer.valueOf(3), actualResponse.getSummary().getTotal());
    assertEquals("001", actualResponse.getQuestions().get(0).getQuestionId());
    assertEquals("002", actualResponse.getQuestions().get(1).getQuestionId());
    assertEquals("003", actualResponse.getQuestions().get(2).getQuestionId());
  }
  
  private String resolveFileAsString(String input) {
    final URL resource = Resources.getResource(input);
    try {
      return Resources.toString(resource, StandardCharsets.UTF_8).trim();
    } catch (IOException e) {
      throw new IllegalArgumentException(e);
    }
  }
}